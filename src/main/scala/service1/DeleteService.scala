package service1

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Dataset, SparkSession}
import entities.{Client, Path}
import org.apache.spark.sql.types.{IntegerType, LongType}

object DeleteService {

  def read(sparkSession: SparkSession, path: String): Dataset[Client] = {

    sparkSession.sparkContext.setCheckpointDir("tmp")

    import sparkSession.implicits._
    val dt: Dataset[Client] = sparkSession
      .read
      .option("header", true)
      .option("delimiter", ";")
      .csv(path).coalesce(1)
      .withColumn("IdentifiantClient", 'IdentifiantClient.cast(LongType))
      .as[Client]
    dt
  }

  def write(path: String, ds: Dataset[Client]): Unit = {
    ds.checkpoint(true)
      .write
      .option("header", true)
      .option("delimiter", ";")
      .mode("overwrite")
      .csv(path)
  }

  def deleteClient(sparkSession: SparkSession, id: Long): Unit = {
    val file = Path.getHdfsLocal()
    val data = read(sparkSession, file)
    val dt = data.filter(data("IdentifiantClient") =!= (id))
    write(file, dt)
  }

}
