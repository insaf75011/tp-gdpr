package service2

import org.apache.spark.sql.functions.{base64, col, hash, udf, when}
import org.apache.spark.sql.types.{LongType, StringType}
import org.apache.spark.sql.{Dataset, SparkSession}
import entities.{Client, Path}


object HashService {

  def read(sparkSession: SparkSession, path: String): Dataset[Client] = {

    sparkSession.sparkContext.setCheckpointDir("tmp")

    import sparkSession.implicits._
    val dt: Dataset[Client] = sparkSession
      .read
      .option("header", true)
      .option("delimiter", ";")
      .csv(path).coalesce(1)
      .withColumn("IdentifiantClient", 'IdentifiantClient.cast(LongType))
      .as[Client]
    dt
  }

  def write(path: String, ds: Dataset[Client]): Unit = {
    ds.checkpoint(true)
      .write
      .option("header", true)
      .option("delimiter", ";")
      .mode("overwrite")
      .csv(path)
  }

  def hashData(sparkSession: SparkSession, dataset: Dataset[Client], id:Long): Dataset[Client] = {

    import sparkSession.implicits._

    val nom = dataset.withColumn("Nom", when(col("IdentifiantClient").equalTo(id), base64(col("Nom")))
      .otherwise(col("Nom"))).as[Client]
    val prenom = nom.withColumn("Prenom", when(col("IdentifiantClient").equalTo(id), base64(col("Prenom")))
      .otherwise(col("Prenom"))).as[Client]
    val adresse = prenom.withColumn("Adresse", when(col("IdentifiantClient").equalTo(id), base64(col("Adresse")))
      .otherwise(col("Adresse"))).as[Client]
    adresse
  }

  /*def hashNom(dataset: Dataset[Client], id:Long): Unit = {
    dataset.withColumn("Nom", when(col("IdentifiantClient").equalTo(id), base64(col("IdentifiantClient").cast(StringType)))
      .otherwise(col("IdentifiantClient")
      ))
  }*/

   /* val dt = dataset

    val nom = dataset.withColumn("nomh", base64(col("nom")))
    val prenom = nom.withColumn("phash", base64(nom.col("prenom")))
    val adresse = prenom.withColumn("adresseh", base64(dt.col("adresse")))

    val hash = adresse.select("IdentifiantClient", "nomh", "phash", "adresseh", "DateDeSouscription")*/


    def hashClient(sparkSession: SparkSession, id: Long): Unit = {
      val file = Path.getHdfsLocal()
      val data = read(sparkSession, file).coalesce(1)
      val hashDataset = hashData(sparkSession, data, id)
      write(file, hashDataset)
    }

}
