import entities.Config
import org.apache.spark.sql.SparkSession
import scopt.{OParser, OParserBuilder}
import service1.DeleteService
import service2.HashService

import scala.sys.exit

object Main {

  val builder: OParserBuilder[Config] = OParser.builder[Config]
  val argParser: OParser[Unit, Config] = {
    import builder._
    OParser.sequence(
      programName("tp-gdpr"),
      head("tp-gdpr", "1.0.0"),
      opt[Long]('d', "delete")
        .optional()
        .action((s, c) => c.copy(delete = s))
        .text("delete by id")
        .validate(s =>
          if (s <= 1000 && s > 0) success
          else failure("Id doesn't exist"))
      ,
      opt[Long]('h', "hash")
        .optional()
        .action((s, c) => c.copy(hash = s))
        .text("hash by id")
        .validate(s =>
          if (s <= 1000 && s > 0) success
          else failure("Id doesn't exist"))
    )
  }

  def main(args: Array[String]): Unit = {
    OParser.parse(argParser, args, Config()) match {
      case Some(config) =>
        val delete = config.delete
        val hash = config.hash
        val sparkSession = SparkSession.builder()
          .appName("tp-gdpr")
          .master("local")
          .getOrCreate()
        if (delete > 1) {
          DeleteService.deleteClient(sparkSession, delete)
        }
        if (hash > 1) {
          HashService.hashClient(sparkSession, hash)
        }
      case _ =>
        exit(1)
    }
  }

}

