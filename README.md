Ce projet permet de supprimer les données d'un Client en insérant son id (argument -d=Id ou --delete=Id) ou de les hasher (argument -h=Id ou --hash=Id)

Le premier service concerne la suppression et le second, le hashage.

Il prend comme source un fichier csv de 

Groupe : Insaf et Mehdi

Nos données avant supression d'un id en local :
![Alt text](src/main/resources/before delete.png)
et dans hdfs :
![](./src/main/resources/before delete hdfs.png)

Apres supression en local :
![](src/main/resources/after delete.png)
et dans hdfs :
![](src/main/resources/after delete hdfs.png)

Nos données avant hashage en local :
![](src/main/resources/before hash.png)
et dans hdfs :
![](src/main/resources/before delete hdfs.png)

Après hashage en local :
![](src/main/resources/after delete.png)
et dans hdfs :
![](src/main/resources/after delete hdfs.png)
